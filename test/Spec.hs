import Test.Tasty
import Test.Tasty.HUnit

import BinaryTree

linkedList = add 6 $ add 5 $ add 4 $ add 3 $ add 2 $ add 1 Empty 
tree1 = add (-6) $ add 11 $ add (-5) $ add 5 $ add 2 $ add 9 Empty 
binaryTreeTests = testGroup "Unit tests for BinaryTree"
  [ testCase "empty tree does not contain anything" $ contains 1 Empty @?= False
  , testCase "one node tree contains one thing" $ contains 5 (add 5 Empty) @?= True
  , testCase "one node tree does not contain everything" $ contains 3 (add 9 Empty) @?= False
  , testCase "two node tree contains two things" $ do let tree = add 2 $ add 9 Empty
                                                      contains 2 tree && contains 9 tree @?= True
  , testCase "two node tree does not contain everything" $ contains 3 (add 5 $ add 2 Empty) @?= False
  , testCase "empty tree should have length zero" $ size Empty @?= 0
  , testCase "one node tree should have size 1" $ size (add 2 Empty) @?= 1
  , testCase "several node tree should have size 1" $ size (add 2 Empty) @?= 1
  , testCase "several node tree should have correct size" $ size tree1 @?= 6 
  , testCase "No duplicates should exist in tree" $ size (add 2 $ add 2 Empty) @?= 1
  , testCase "Empty tree should have height zero" $ height Empty @?= 0
  , testCase "one node tree should have height one" $ height (add 2 Empty) @?= 1
  , testCase "two node tree should have height two" $ height (add 5 (add 2 Empty)) @?= 2
  , testCase "several node tree should have correct height" $ height tree1 @?= 4
  , testCase "degenerate tree should have same height as size" $ height linkedList @?= size linkedList
  , testCase "remove node from empty tree should still be empty" $ remove 5 Empty @?= (Empty :: BinaryTree Int)
  , testCase "remove node from one node tree should be empty" $ remove 5 (add 5 Empty) @?= (Empty :: BinaryTree Int)
  , testCase "remove no node from one node tree should have same size" $ size (remove 5 (add 3 Empty)) @?= 1
  , testCase "remove no node from one node tree should contain that node" $ contains 3 (remove 5 (add 3 Empty)) @?= True
  , testCase "remove node from two node tree should have size one" $ size (remove 5 (add 4 (add 5 Empty))) @?= 1
  , testCase "remove node from degenerate tree should reduce size by one" $ size (remove 4 linkedList) @?= 5
  , testCase "remove node from several node tree should reduce size by one" $ size (remove 2 tree1) @?= 5
  , testCase "remove no node from several node tree should have same size" $ size (remove 122 tree1) @?= 6
  , testCase "remove node from several node tree should still contain all other nodes" $ (contains 5 (remove 9 tree1)) &&
                                                                                         (contains 11 (remove 9 tree1)) &&
                                                                                         (contains 2 (remove 9 tree1)) &&
                                                                                         (contains (-5) (remove 9 tree1)) &&
                                                                                         (contains (-6) (remove 9 tree1)) @?= True
  , testCase "remove node from several node tree should not contain removed node" $ contains (-5) (remove (-5) tree1) @?= False
  , testCase "Empty tree to list should be empty list" $ toSortedList Empty @?= ([] :: [Int])
  , testCase "one node tree to list should be one element list" $ toSortedList (add 3 Empty) @?= [3]
  , testCase "several node tree to list should return correct list" $ toSortedList tree1 @?= [-6, -5, 2, 5, 9, 11]
  , testCase "degenerate tree to list should return correct list" $ toSortedList linkedList @?= [1..6]
  , testCase "balance empty tree should still be empty" $ balance (Empty :: BinaryTree Int) @?= (Empty :: BinaryTree Int)
  , testCase "balance one node tree should have size one" $ size (balance (add 5 Empty)) @?= 1
  , testCase "balance one node tree should contains correct node" $ contains 5 (balance (add 5 Empty)) @?= True
  , testCase "balance one node tree should not contains wrong node" $ contains 3 (balance (add 5 Empty)) @?= False
  , testCase "degenerate tree should have correct height after balance" $ height (balance linkedList) @?= 3
  , testCase "balanced tree should have same size" $ size (balance linkedList) @?= size linkedList
  , testCase "Several node tree should have correct height after balance" $ height (balance tree1) @?= 3
  ]

main = defaultMain binaryTreeTests
