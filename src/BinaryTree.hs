module BinaryTree
    ( contains
    , add
    , size
    , height
    , remove
    , toSortedList
    , balance
    , BinaryTree (Empty)
    ) where

import Data.DList
import qualified Data.Vector as Vector

data BinaryTree a = Empty | Node a (BinaryTree a) (BinaryTree a) deriving (Show, Eq)

contains :: Ord a => a -> BinaryTree a -> Bool
contains _ Empty = False
contains e (Node n left right) | e == n = True
                                     | e < n = contains e left
                                     | otherwise = contains e right

add :: Ord a => a -> BinaryTree a -> BinaryTree a
add e Empty = Node e Empty Empty
add e tree@(Node n left right) | e == n = tree
                               | e < n = Node n (add e left) right
                               | otherwise = Node n left (add e right)

size Empty = 0
size (Node _ left right) = 1 + size left + size right

height Empty = 0
height (Node a left right) = 1 + max (height left) (height right)

remove _ Empty = Empty
remove e tree@(Node a left right)  | e == a = removeRoot tree
                                   | e < a = Node a (remove e left) right
                                   | otherwise = Node a left (remove e right)
removeRoot (Node a Empty Empty) = Empty
removeRoot (Node a left Empty) = left
removeRoot (Node a Empty right) = right
removeRoot tree@(Node a left right) = replaceRootWithInOrderSuccessor tree (leftMostChild right)

replaceRootWithInOrderSuccessor (Node a left right) (Node s Empty Empty) = Node s left (removeLeftMostChild right)
replaceRootWithInOrderSuccessor (Node a left right) (Node s Empty (Node f Empty Empty)) = Node s left (add f (removeLeftMostChild right))

leftMostChild tree@(Node a Empty _) = tree
leftMostChild (Node a left _) = leftMostChild left

removeLeftMostChild (Node a Empty _) = Empty
removeLeftMostChild (Node a left right) = Node a (removeLeftMostChild left) right

toSortedList tree = toList $ toSortedDList tree

toSortedDList :: Ord a => BinaryTree a -> DList a
toSortedDList Empty = empty
toSortedDList (Node a left right) = (toSortedDList left) `append` (singleton a) `append` (toSortedDList right)

balance :: Ord a => BinaryTree a -> BinaryTree a
balance Empty = Empty
balance tree = fromListToTree $ toSortedList tree

fromListToTree :: Ord a => [a] -> BinaryTree a
fromListToTree list = fromVectorToTree vector 0 ((length vector) - 1)
  where vector = Vector.fromList list

fromVectorToTree :: Ord a => Vector.Vector a -> Int -> Int -> BinaryTree a
fromVectorToTree vector min max | Vector.null vector || min > max = Empty
                                | otherwise = Node root (fromVectorToTree vector min (min + half - 1)) (fromVectorToTree vector (min + half + 1) max)
  where root = vector Vector.! (min + half)
        half = ceiling ((fromIntegral (max - min)) / 2)
