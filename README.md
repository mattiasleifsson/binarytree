# BinaryTree
This project contains a basic implementation of a Binary search
tree in Haskell. It is not self-balancing, but instead a function
`balance` is provided, which will balance a tree in linear time
on demand.
